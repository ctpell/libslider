/* LIBSLIDER PYRAMID THREE v 1.0
 *
 * Pyramid slider example with active slide on top and 3 slides height for LIBSLIDER
 *
 * by Volodymyr Golykov
 *
 * MIT License
 * Copyright (c) 2015 Volodymyr Golykov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 */
LIBSLIDER.types['pyramid_three'] = {
	display: -3,
	drag_type: 'drag',
	styles: function ( styles, slide ) {
		if ( slide == 0 ) {
			styles = {
				left: 115,
				top: 70,
				width: 225,
				height: 150,
				'z-index': 4
			};
		} else if ( Math.abs(slide) == 1 ) {
			styles = {
				left: slide > 0 ? 250 : 50,
				top: 95,
				width: 150,
				height: 100,
				'z-index': 3
			};
		} else if ( Math.abs(slide) == 2 ) {
			styles = {
				left: slide > 0 ? 320 : 10,
				top: 105,
				width: 120,
				height: 80,
				'z-index': 2
			};
		} else {
			styles = {
				left: 180,
				top: 112,
				width: 100,
				height: 67,
				'z-index': 1
			};
		}
		return styles;
	},
	drag: function ( slide_delta, drag ) {
		return - drag.delta.x / 225;
	}
};