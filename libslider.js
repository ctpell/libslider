/**
 * LIBSLIDER v 1.1
 *
 * jQuery slider library
 *
 * by Volodymyr Golykov
 *
 * MIT License
 * Copyright (c) 2015 Volodymyr Golykov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 */


// GLOBAL VARIABLE
var LIBSLIDER = {
	instances: [],	// Array of all defined libslider instances
	types: {},		// list of predefined configs
	// Helper function to get instance by DOM element
	find_instance: function ( elem ) {
		for ( var i in this.instances ) {
			if ( this.instances[i].$element.is(elem) ) {
				return this.instances[i];
			}
		}
	},
	// CONSTANTS
	DRAG_TYPE_NONE: 0,
	DRAG_TYPE_DRAG: 1,
	DRAG_TYPE_SWIPE_EVENTS: 2
};

// PROTOTYPE
/**
 * constructor params
 * @param element 	- DOM element which contains all slides, thumbs and controls
 * @param type 		- string / slider predefined type ('none' - for none :)
 * @param options	- array of custom user options
 */
function libslider ( element, type, options ) {
	// Internal variables
	this.slide_current = 0;			// Current slide number (or float in animation process)
	this.slide_start = 0;			// Start slide for animation
	this.slide_start_styles = [];	// Array of slide styles on start (used in direct_switch algorithm)
	this.slide_target = 0;			// Target slide for animation
	this.slide_nohack = 0;			// Temporary disables slide number calculation hack
	this.slides_count = 0;			// Original slides count
	this.slides_count_cloned = 0;	// Count of cloned slides
	this.slides_count_visible = 0;	// Number of visible slides
	this.animate_percent_step = 0;	// Step size for animation in percents
	this.animate_timer = null;		// Timer ID from setTimeout used in animation
	this.animate_percent = 0;		// Current animation progress percent
	this.drag = {					// Drag status object
		is: false,					// Drag is now performing
		prev_timestamp: 0,			// Timestamp pf previous drag event
		start: {					// Mouse coordinates where drag started
			x: 0,
			y: 0
		},
		current: {					// Current mouse coordinates
			x: 0,
			y: 0
		},
		prev: {						// Mouse coordinated in prevous drag event
			x: 0,
			y: 0
		},
		delta: {					// Mouse delta between start and current
			x: 0,
			y: 0
		},
		velocity: {					// Current mouse velocity
			x: 0,
			y: 0
		},
		velocity_log: []			// Velocity data array of some previous drag events (to calculate local max and average)
	};
	this.autorotate_timer = null;	// Timer ID for autorotation

	this.$element = $(element);		// jQuery instance of main element
	this.type = type;				// predefined type string

	// Default common options
	this.options = {
		start_slide: 0,					// Start slide ( 0 - for first )
		prev_next_step: 1,				// Slider previous/next button step
		carousel: false,				// Enabling carousel (infinite slider)
		carousel_clone: false,			// Enable carousel cloning and display fill

		autorotate: false,				// Automatic slider rotation
		autorotate_step: 1,				// Autorotate step
		autorotate_delay: 1000,			// Delay between automatic rotations
		autorotate_hover_pause: true,	// Pause autorotation on hover (see also this.options.sel_pause)

		static_switch_on : 0.5,			// percent value, where animation switches static css properties (such as z-index) default to 50% (0.5)
		//overlap: 0,					// float @todo decide if we need this
										// slides A => B
										// 0 - all slides start animation simultaneously
										// >0 - A starts animation earlier than B
										// 1 - A finishes animation at the same time when B starts it
										// <0 - B starts animation earlier then A
										// -1 - B finishes animation a the same time when A starts it
		animate_step: 13,				// animation step interval in ms
		display: 0,						// Number of visible slides
										// Number A
										// A = 0 - all slides are visible
										// A > 0 - visible CUR .. CUR+A-1
										// A < 0 - visible CUR-A+1 .. CUR+A-1
										// Array [A,B] - visible CUR-A+1 .. CUR+B-1
										// NOTE: in direct_switch mode all slides are always visible
										//       but this value is used to calculate carousel_clone
		display_edge: 1,				// controls slider visible edges
										// Ignored in carousel mode
										// Variants same as display

		active_position: 0,				// Shift of active slide
		direct_switch: false,			// false 1 -> 2 -> 3
										// true  1 -> 3

		drag_type: 'none',				// Drag type
										// 'none' - drag disabled
										// 'drag' - drag enabled
										// 'swipe_events' - slider will fire goto next and goto prev on swipe events
										//					by default events are swipeleft and swiperight from jquery mobile

		drag_cross_over: 0.49,			// Shift of end bounds for drag_type = drag and carousel = false
		drag_swipe: 'x',				// Swipe direction for drag_type = drag
										//	'x' - x-axis swipe
										// 	'y' - y-axis swipe
										// 	'none' - swiping disabled
		drag_swipe_min_distance: 40,	// Minimum distance in pixels for swiping
		drag_swipe_min_velocity: 0.3,	// Minimum end velocity for swiping
		drag_velocity_log_width: 4,		// Velocity log depth for calculating max velocity in the end of drag for swiping

		cache_styles: true,				// Caching styles for each slide delta (useful when slide positions are static)

		sel_slides: '',					// Selector for all slides DOMs
		sel_prev: '',					// Selector for GotoPervious DOMs
		sel_next: '',					// Selector for GotoNext DOMs
		sel_thumbs: '',					// Selector for slider thumb DOMs
		sel_drag: '',					// Selector for slider drag area (defaults to whole slider)
		sel_pause: '',					// Selector for on:hover autorotate pausing (defaults to whole slider)

		class_active: 'active',			// Active slide additional class
		class_thumb_active: '',			// Active thumb additional class (defaults to class_active)
		class_cloned: 'cloned',			// Additional class for cloned carousel elements
		class_prev_next_disabled: 'disabled', // Additional class for sel_prev and sel_next elements when they're disabled

		duration: 500,					// Slider animation duration in ms
		//queue: false, // @todo create queue if needed
		easing: 'swing',				// jQuery easing for animation

		event_thumbs_switch: 'click',		// jQuery event for thumb switch
		event_prev_next_switch: 'click',	// jQuery event for prev/next switch
		event_mousedown: 'mousedown',		// Mousedown event for drag (can be replaced with vmousedown from jquery.mobile)
		event_mousemove: 'mousemove',		// Mousemove event for drag (can be replaced with vmousemove from jquery.mobile)
		event_mouseup: 'mouseup dragend',	// Mouseup event for drag (can be replaced with vmouseup from jquery.mobile)
		event_mouseover: 'mouseover',		// MouseOver event for autorotate pause (can be replaced with vmouseover from jquery.mobile)
		event_mouseout: 'mouseout',			// MouseOut event for autorotate unpause (can be replaced with vmouseout from jquery.mobile)
		event_swipe_prev: 'swipeleft',		// Go to previous swipe event for drag_type = 'swipe_events'
		event_swipe_next: 'swiperight'		// Go to next swipe event for drag_type = 'swipe_events'

		/* HOOKS
		init: function () {
			Fires after all default init procedures
			@params
				this - current lib_slider instance
		},
		styles: function ( styles, slide ) {
			Fires when slider trying to get CSS style for particular slide
			@params
				this - current lib_slider instance
				styles - object with current CSS styles of slide (as for jQuery.css function)
				slide - slide number (0 - for 1st)
			@return
				styles object
		},
		drag: function ( slide, drag ) {
			Fires on each drag event
			@params
				this - current lib_slider instance
				slide - current slider position (float)
				drag - current drag object (@see this.drag)
			@return
				slide_delta - calculated slider position
		}
		*/
	};
	// List of available hooks
	this.hooks = {
		init: [],
		styles: [],
		drag: []
	};
	// Caches
	this.cache = {
		styles: []
	};
	// Checking predefined slider type
	if ( typeof (LIBSLIDER.types[type]) != 'object' ) {
		if ( type != 'none' ) {
			console.error ( 'Slider with type '+type+' not found!' );
		}
		LIBSLIDER.types[type] = {};
	}
	// Resolving hooks
	for ( var hook in this.hooks ) {
		if ( typeof ( LIBSLIDER.types[type][hook] ) == 'function' ) {
			this.hooks[hook].push ( LIBSLIDER.types[type][hook] );
		}
		if ( typeof ( options[hook] ) == 'function' ) {
			this.hooks[hook].push ( options[hook] );
		}
	}
	// Extend options
	$.extend ( this.options, LIBSLIDER.types[type] );
	$.extend ( this.options, options );

	// Adding new instance to global instances array
	LIBSLIDER.instances[LIBSLIDER.instances.length] = this;

	// Init slider
	this.init();
}
/**
 * HOOK helper
 * @param hook		hook name
 * @param retval	return value
 * @returns {*}
 */
libslider.prototype.hook = function ( hook, retval ) {
	for ( var i in this.hooks[hook] ) {
		if ( typeof ( this.hooks[hook][i] ) == 'function' ) {
			var args = Array.prototype.slice.call(arguments);
			args = args.slice ( 1 );
			retval = this.hooks[hook][i].apply(this, args);
		}
	}
	return retval;
}
/**
 * Merges new options object with existed
 * @param options - new option object
 */
libslider.prototype.options_merge = function ( options ) {
	$.extend ( this.options, options );
};
/**
 * Helper function to get option value
 * @param option - option name
 * @returns {*}
 */
libslider.prototype.options_get = function ( option ) {
	if ( typeof option != 'undefined' )
		return this.options[option];
	else
		return this.options;
};
/**
 * Checking and validating some this.options
 */
libslider.prototype.resolve_options = function () {
	// DRAG TYPE
	this.options.drag_type = this.resolve_option ( this.options.drag_type, {
		'none': LIBSLIDER.DRAG_TYPE_NONE,
		'drag': LIBSLIDER.DRAG_TYPE_DRAG,
		'swipe_events': LIBSLIDER.DRAG_TYPE_SWIPE_EVENTS
	}, LIBSLIDER.DRAG_TYPE_NONE, 'unrecognized drag type' );
	// DRAG SWIPE
	if ( this.options.drag_type ) {
		this.options.drag_swipe = this.resolve_option ( this.options.drag_swipe, {
			'none': false,
			'x': 'x',
			'y': 'y'
		}, 'x', 'wrong drag swipe axis!' );
	}
};
/**
 * Helper function for this.resolve_options()
 * @param option			option value, defined by user
 * @param known_options		available options
 * @param def				default value if case of error
 * @param error				error string to display in console
 * @returns 				validated option
 */
libslider.prototype.resolve_option = function ( option, known_options, def, error ) {
	var found = false;
	if ( typeof(option) == 'string' ) {
		option = option.toLowerCase();
	}
	for ( var type in known_options ) {
		if ( option == type || option == known_options[type] ) {
			option = known_options[type];
			found = true;
			break;
		}
	}
	if ( !found ) {
		console.warn( 'libslider: '+error+': '+this.options.drag.type );
		option = def;
	}
	return option;
};
/**
 * Resolvs variable by display rules
 * @param display
 * @returns normalized display option [A;B]
 */
libslider.prototype.resolve_display = function ( display ) {
	if ( typeof ( display ) == 'object' ) {
		return display;
	} else if ( typeof ( display ) == 'number' ) {
		if ( display == 0 ) {
			return [0,9999];
		} else if ( display > 0 ) {
			return [0,display-1];
		} else {
			return [-display-1,-display-1];
		}
	} else {
		return [0,9999];
	}
}
/**
 * Slider init
 */
libslider.prototype.init = function () {
	this.resolve_options();
	if ( !this.options.sel_slides ) {
		console.error ( 'libslider: slides selector not specified!' );
	}
	this.$slides = this.$element.find(this.options.sel_slides);
	this.slides_count = this.$slides.length;
	this.$prev = this.$element.find(this.options.sel_prev);
	this.$next = this.$element.find(this.options.sel_next);
	this.$thumbs = this.$element.find(this.options.sel_thumbs);
	if ( this.slides_count % this.$thumbs.length != 0 ) {
		console.warn ( 'libslider: count of slides and thumbs does not match!' );
	}
	if ( !this.options.sel_drag ) {
		this.$drag = this.$element;
	} else {
		this.$drag = this.$element.find(this.options.sel_drag);
	}

	if ( !this.options.class_thumb_active ) {
		this.options.class_thumb_active = this.options.class_active;
	}

	// DISPLAY
	this.options.display = this.resolve_display ( this.options.display );
	this.options.display_edge = this.resolve_display ( this.options.display_edge );
	this.slides_count_visible = this.options.display[0] + this.options.display[1] + 1;
	if ( this.options.carousel == 'auto' ) {
		this.options.carousel = this.slides_count > this.slides_count_visible;
	}

	// CALCULATING NEW DISPLAY FOR FEW SLIDES
	if ( this.options.carousel && !this.options.carousel_clone ) {
		var l = 0, r = 0, placed = 1;
		for ( var delta = 1; delta <= Math.max(this.options.display[0],this.options.display[1]); delta ++ ) {
			if ( placed >= this.slides_count ) break;	// all slides are placed (hits when there are to few slides)
			for ( var sign = 0; sign < 2; sign ++ ) {
				if ( placed >= this.slides_count ) break;	// all slides are placed (hits when there are to few slides)
				var delta2 = sign ? -delta : delta;
				if ( !this.display ( delta2 ) ) continue;
				placed ++;
				if ( sign ) {
					l ++;
				} else {
					r ++;
				}
			}
		}
		this.options.display = [l,r];
		this.slides_count_visible = this.options.display[0] + this.options.display[1] + 1;
	}

	// CAROUSEL cloning
	if ( this.options.carousel && this.options.carousel_clone ) {
		for ( var slide = -this.options.display[0]; slide <= this.options.display[1]+1; slide ++ ) {
			if ( slide == 0 ) continue;
			var slide_hacked = this.slide_number_circle_hack ( slide );
			var slide_to_clone = slide < 0 ? slide_hacked : slide_hacked - 1;
			var slide_delta = slide < 0 ? slide : slide + this.slides_count - 1;
			var $cs = this.$slides.eq(slide_to_clone).clone();
			$cs.data ( 'libslider_slide_clone_delta', slide_delta );
			$cs.addClass(this.options.class_cloned);
			this.$slides.parent().append($cs);
			this.slides_count_cloned ++;
		}
		// Updating slides var
		this.$slides = this.$element.find(this.options.sel_slides);
		//this.slides_count = this.$slides.length;
	}


	// BINDING EVENTS
	if ( this.options.drag_type == LIBSLIDER.DRAG_TYPE_DRAG ) {
		this.$drag.on ( this.options.event_mousedown, {slider: this}, function (e) {
			e.data.slider.event_mousedown ( e );
			return false;
		});
		$(document).on ( this.options.event_mousemove, {slider: this}, function (e) {
			e.data.slider.event_mousemove ( e );
		});
		$(document).on ( this.options.event_mouseup, {slider: this}, function (e) {
			e.data.slider.event_mouseup ( e );
		});
	} else if ( this.options.drag_type == LIBSLIDER.DRAG_TYPE_SWIPE_EVENTS ) {
		this.$drag.on ( this.options.event_swipe_prev, { slider: this }, function (e) {
			e.data.slider.goto_shift ( 1 );
		});
		this.$drag.on ( this.options.event_swipe_next, { slider: this }, function (e) {
			e.data.slider.goto_shift ( -1 );
		});
		this.$drag.on ( this.options.event_mousedown, {slider: this}, function (e) {
			return false;
		});
	}
	if ( this.options.autorotate ) {
		if ( this.options.autorotate_hover_pause ) {
			if ( !this.options.sel_pause ) {
				this.$pause = this.$element;
			} else {
				this.$pause = this.$element.find(this.options.sel_pause);
			}
			this.$pause.on( this.options.event_mouseover, { slider: this }, function(e) {
				e.data.slider.autorotate_stop();
			});
			this.$pause.on( this.options.event_mouseout, { slider: this }, function(e) {
				e.data.slider.autorotate_start();
			});
		}
	}

	this.$thumbs.on ( 'mousedown', function (e) {
		return false;
	});
	this.$thumbs.on ( this.options.event_thumbs_switch, { slider: this }, function (e) {
		var slide = $(this).parent().find(e.data.slider.options.sel_thumbs).index ( this );
		e.data.slider.goto ( slide );
	});
	this.$next.on ( this.options.event_prev_next_switch, { slider: this }, function (e) {
		if ( !$(this).hasClass(e.data.slider.options.class_prev_next_disabled)) {
			e.data.slider.goto_shift ( e.data.slider.options.prev_next_step );
		}
	});
	this.$prev.on ( this.options.event_prev_next_switch, { slider: this }, function (e) {
		if ( !$(this).hasClass(e.data.slider.options.class_prev_next_disabled)) {
			e.data.slider.goto_shift ( - e.data.slider.options.prev_next_step );
		}
	});

	// HOOK INIT
	this.hook ( 'init' );

	// SETTING SLIDER START SLIDE
	this.goto ( this.options.start_slide, 'teleport', true );	// hack to init styles to all slides
	this.goto ( this.options.start_slide, 'teleport' );			// now we init using display
};
/**
 * MOUSEDOWN drag handler
 * @param e - event object
 */
libslider.prototype.event_mousedown = function ( e ) {
	this.drag.is = true;
	this.animate_stop();
	this.autorotate_stop();
	if ( !this.options.carousel ) {
		this.slide_nohack = true;
	}
	this.drag.prev_timestamp = (new Date).getTime();
	this.drag.start.x = e.pageX;
	this.drag.start.y = e.pageY;
	this.drag.prev.x = e.pageX;
	this.drag.prev.y = e.pageY;
	this.drag.velocity.x = 0;
	this.drag.velocity.y = 0;
	this.drag.velocity_log = [];
	this.slide_start = this.slide_current;
};
/**
 * MOUSEMOVE drag handler
 * @param e - event object
 */
libslider.prototype.event_mousemove = function ( e ) {
	if ( this.drag.is ) {
		this.drag.current.x = e.pageX;
		this.drag.current.y = e.pageY;
		this.drag.delta.x = this.drag.current.x - this.drag.start.x;
		this.drag.delta.y = this.drag.current.y - this.drag.start.y;
		// VELOCITY HANDLING
		var timestamp = (new Date).getTime();
		var time_diff = ( timestamp - this.drag.prev_timestamp );
		var vel = {
			x: ( this.drag.current.x - this.drag.prev.x ) / time_diff,
			y: ( this.drag.current.y - this.drag.prev.y ) / time_diff
		};
		this.drag.velocity = vel;
		this.drag.prev_timestamp = timestamp;
		this.drag.prev.x = this.drag.current.x;
		this.drag.prev.y = this.drag.current.y;
		this.drag.velocity_log.push ( vel );
		if ( this.drag.velocity_log.length > this.options.drag_velocity_log_width ) {
			this.drag.velocity_log.shift();
		}
		this.slide_current = this.slide_start + this.on_drag();
		if ( !this.options.carousel ) {
			if ( this.slide_current > this.slides_count - this.options.display_edge[0] - 1 + this.options.drag_cross_over ) {
				this.slide_current = this.slides_count - this.options.display_edge[0] - 1 + this.options.drag_cross_over;
			} else if ( this.slide_current < -this.options.drag_cross_over + this.options.display_edge[1] ) {
				this.slide_current = -this.options.drag_cross_over + this.options.display_edge[1];
			}
		}
		this.goto_float();
	}
};
/**
 * Helper function that calculates max velocity from log values
 * @returns {{x: number, y: number}}
 */
libslider.prototype.get_max_vel_from_log = function () {
	var m = {
		x: 0,
		y: 0
	};
	for ( var i = 0; i < this.drag.velocity_log.length; i ++ ) {
		if ( Math.abs(this.drag.velocity_log[i].x) > Math.abs(m.x) ) {
			m.x = this.drag.velocity_log[i].x;
		}
		if ( Math.abs(this.drag.velocity_log[i].y) > Math.abs(m.y) ) {
			m.y = this.drag.velocity_log[i].y;
		}
	}
	return m;
};
/**
 * MOUSEUP drag event handler
 * @param e - event object
 */
libslider.prototype.event_mouseup = function ( e ) {
	if ( this.drag.is ) {
		this.drag.is = false;
		var target;
		var target_add = 0;
		if ( this.options.drag_swipe && Math.round(this.slide_start) == Math.round (this.slide_current) ) {
			if ( Math.abs(this.drag.delta[this.options.drag_swipe]) > this.options.drag_swipe_min_distance ) {
				var max_vel = this.get_max_vel_from_log();
				if ( Math.abs(max_vel[this.options.drag_swipe]) > this.options.drag_swipe_min_velocity ) {
					target_add = this.drag.delta[this.options.drag_swipe] > 0 ? -1 : 1;
				}
			}
		}

		target = this.slide_current + target_add;
		if ( target > this.slides_count - this.options.display_edge[0] - 1 && !this.options.carousel ) {
			target = this.slides_count - this.options.display_edge[0] - 1;
		} else if ( target < this.options.display_edge[1] && !this.options.carousel ) {
			target = this.options.display_edge[1];
		} else {
			target = Math.round ( target );
		}

		this.goto ( target );
	}
};
/**
 * Calculating slide_delta from drag hooks
 * @returns {number}
 */
libslider.prototype.on_drag = function () {
	return this.hook ( 'drag', 0, this.drag );
};
/**
 * Checks if slide is visible or not (@see this.options.display)
 * @param delta - slide_delta
 * @returns {boolean}	- visible or not
 */
libslider.prototype.display = function ( delta ) {
	return delta > -this.options.display[0]-1 && delta < this.options.display[1]+1;
};
/**
 * Calculating styles object from hooks
 * @param slide	- slide number
 * @returns {object}
 */
libslider.prototype.styles = function ( slide ) {
	slide += this.options.active_position;
	if ( typeof ( this.cache.styles[slide] == 'indefined' ) || !cache_styles ) {
		var styles = {};
		styles = this.hook ( 'styles', styles, slide );
		this.cache.styles[slide] = styles;
	}
	return this.cache.styles[slide];
};

/**
 * Autorotate callback (goto next slide)
 */
libslider.prototype.autorotate = function () {
	var next = this.slide_target + this.options.autorotate_step;
	if ( !this.options.carousel ) {
		if ( next >= this.slides_count ) {
			next -= this.slides_count;
		}
	}
	this.goto ( next );
};

/**
 * Starts, resumes autorotation
 */
libslider.prototype.autorotate_start = function () {
	this.autorotate_stop();
	var T = this;
	this.autorotate_timer = setTimeout ( function () {
		T.autorotate();
	}, this.options.autorotate_delay );
}

/**
 * Stops autorotation
 */
libslider.prototype.autorotate_stop = function () {
	clearTimeout ( this.autorotate_timer );
}

/**
 * Go to current slide + shift
 * @param shift
 */
libslider.prototype.goto_shift = function ( shift ) {
	if ( !shift ) return;
	var next = this.slide_target + shift;
	if ( !this.options.carousel ) {
		if ( next >= this.slides_count - this.options.display_edge[1] ) {
			next = this.slides_count - this.options.display_edge[1] - 1;
		} else if ( next <= this.options.display_edge[0] ) {
			next = this.options.display_edge[0];
		}
	}
	this.goto ( next );
};
/**
 * Go to slide
 * @param slide - slide bumber
 * @param type 'animate' - using animation, 'teleport' - immediately
 * @param display_override - true - all slides are visible, false - this.display controls visibility
 */
libslider.prototype.goto = function ( slide, type, display_override ) {
	if ( !this.options.carousel ) {
		slide = this.slide_number_circle_hack ( slide, 1 );
	}
	if ( typeof (type) == 'undefined' ) type = 'animate';
	if ( type == 'teleport' ) {
		this.slide_target = this.slide_current = slide;
		this.goto_float ( display_override );
	} else {
		this.animate ( slide );
	}
	this.class_change();
	if ( this.options.autorotate ) {
		this.autorotate_start();
	}
};

/**
 * Go to float position of this.slide_current
 * @param display_override - true - all slides are visible, false - this.display controls visibility
 */
libslider.prototype.goto_float = function ( display_override ) {
	var current = this.slide_number_circle_hack ( this.slide_current );
	for ( var delta = 0; delta < this.slides_count + this.slides_count_cloned; delta ++ ) {
		var delta2 = delta;
		if ( this.options.carousel && this.options.carousel_clone ) {
			if ( this.$slides.eq(delta).hasClass ( this.options.class_cloned )) {
				delta2 = this.$slides.eq(delta).data('libslider_slide_clone_delta');
			}
		}
		if ( this.options.carousel && !this.options.carousel_clone ) {
			if ( delta2 - this.slides_count + this.slides_count_visible > this.options.display[1] ) {
				delta2 -= this.slides_count;
			}
		}
		var styles, display;
		var slide_coords = delta2 - current;

		var pre = Math.floor ( slide_coords );
		var percent = slide_coords - pre;
		var pos = Math.ceil ( slide_coords );
		if ( this.options.carousel && !this.options.carousel_clone ) {
			if ( pre - this.slides_count + this.slides_count_visible < -this.options.display[0] ) {
				pre += this.slides_count;
			}
			if ( pre - this.slides_count + this.slides_count_visible > this.options.display[1] ) {
				pre -= this.slides_count;
			}
			if ( pos - this.slides_count + this.slides_count_visible < -this.options.display[0] ) {
				pos += this.slides_count;
			}
			if ( pos - this.slides_count + this.slides_count_visible > this.options.display[1] ) {
				pos -= this.slides_count;
			}
		}
		if ( typeof (display_override) != 'undefined' ) {
			display = display_override;
		} else {
			display = this.display ( pre ) || this.display ( pos );
		}
		if ( display ) {
			styles = this.styles_float ( pre, pos, percent );
		}

		this.apply_styles ( delta, styles, display );
	}
};
/**
 * Animation helper
 */
libslider.prototype.goto_percent = function () {
	var eased_percent = $.easing[this.options.easing]( this.animate_percent );
	this.slide_current = this.slide_start + ( this.slide_target - this.slide_start ) * eased_percent;
	if ( this.options.direct_switch ) {
		for ( var delta = 0; delta < this.slides_count; delta ++ ) {
			var styles;
			var display;

			var target_styles = this.styles_float ( delta - this.slide_target, delta - this.slide_target, 1 );
			display = true;
			if ( typeof (this.slide_start_styles[delta]) == 'undefined' ) {
				styles = target_styles;
			} else {
				var start_styles = this.slide_start_styles[delta];
				styles = this.styles_float_percent ( start_styles, target_styles, eased_percent );
			}

			this.apply_styles ( delta, styles, display );
		}
	} else {
		this.goto_float();
	}
}


/**
 * hack to correct slide number calculation in carousel mode
 * @param slide_num	- real slide_num
 * @param no_cloned	- consider cloned or not
 * @returns {number} - hacked slide_num
 */
libslider.prototype.slide_number_circle_hack = function ( slide_num, no_cloned ) {
	if ( !this.slide_nohack ) {
		no_cloned = typeof(no_cloned) != 'undefined' ? no_cloned : false;
		if ( slide_num < this.options.active_position ) {
			while ( slide_num < ( no_cloned ? 0 : this.options.active_position )) {
				slide_num += this.slides_count;
			}
		} else {
			while ( slide_num > this.slides_count - (no_cloned ? 1 : -this.options.active_position) ) {
				slide_num -= this.slides_count;
			}
		}
	}
	return slide_num;
}
/**
 * Applies styles to particular DOM
 * @param delta		- slide DOM delta
 * @param styles	- styles object
 * @param display	- display DOM or not
 */
libslider.prototype.apply_styles = function ( delta, styles, display ) {
	if ( display ) {
		this.$slides.eq(delta).css(styles);
		this.$slides.eq(delta).show();
	} else {
		this.$slides.eq(delta).hide();
	}
}
/**
 * Checks if property static or not
 * @param prop			- property name
 * @returns {boolean}	- static or not
 */
libslider.prototype.is_static = function ( prop ) {
	return [
		'width',
		'height',
		'max-width',
		'max-height',
		'min-width',
		'min-height',
		'top',
		'right',
		'bottom',
		'left',
		'margin-top',
		'margin-right',
		'margin-bottom',
		'margin-right',
		'padding-top',
		'padding-right',
		'padding-bottom',
		'padding-right',
		'opacity',
		'background-position-x',
		'background-position-y'
	].indexOf(prop) == -1;
};
/**
 * Calculates CSS styles for intermediate slide positions
 * @param float	- slide position (float)
 * @returns {object} - CSS styles object
 */
libslider.prototype.styles_float = function ( pre, pos, percent ) {
	var pos_styles = this.styles ( pos );
	if ( pos == pre )
		return pos_styles;
	var pre_styles = this.styles ( pre );
	return this.styles_float_percent ( pre_styles, pos_styles, percent );
};
/**
 * Helper function to calculate intermediate styles
 * @param pre_styles	- previous integer styles
 * @param pos_styles	- next integer styles
 * @param percent		- percent between previous and next
 * @returns {object} - CSS styles object
 */
libslider.prototype.styles_float_percent = function ( pre_styles, pos_styles, percent ) {
	var cur_styles = {};
	for ( var prop in pos_styles ) {
		if ( this.is_static ( prop ) ) {
			cur_styles[prop] = percent > this.options.static_switch_on ? pos_styles[prop] : pre_styles[prop];
		} else {
			cur_styles[prop] = pre_styles[prop] + (pos_styles[prop] - pre_styles[prop]) * percent;
		}
	}
	return cur_styles;
}
/**
 * Animation step
 * Fires periodically
 */
libslider.prototype.animate_step = function () {
	if ( this.animate_percent + this.animate_percent_step >= 1 ) {
		this.animate_percent = 1;
		this.animate_stop();
	} else {
		this.animate_percent += this.animate_percent_step;
	}
	this.goto_percent ();
}
/**
 * Immediate animation stop
 */
libslider.prototype.animate_stop = function () {
	clearInterval ( this.animate_timer );
	this.animate_timer = null;

	// Preventing float overflow
	this.slide_target = this.slide_current = this.slide_number_circle_hack(this.slide_current);
	this.slide_nohack = false;	// hack on
}
/**
 * Starts new slider animation
 * @param float - destination slider position
 */
libslider.prototype.animate = function ( float ) {
	if ( float == this.slide_current ) return;
	this.slide_start = this.slide_current;
	this.slide_target = float;
	if ( this.options.direct_switch ) {
		var styles = this.styles ( 0 );
		for ( var delta = 0; delta < this.slides_count; delta ++ ) {
			this.slide_start_styles[delta] = {};
			for ( var prop in styles ) {
				this.slide_start_styles[delta][prop] = this.is_static(prop) ? this.$slides.eq(delta).css(prop) : parseFloat (this.$slides.eq(delta).css(prop));
			}
		}
		/// Carousel hack
		this.slide_target = this.slide_number_circle_hack ( this.slide_target );
	}
	this.animate_percent = 0;
	this.animate_percent_step = 1 / Math.round( this.options.duration / this.options.animate_step );
	if ( !this.animate_timer ) {
		var T = this;
		this.animate_timer = setInterval( function () {
			T.animate_step();
		}, this.options.animate_step );
	}
}
/**
 * Applies active classes
 */
libslider.prototype.class_change = function () {
	var target = this.slide_number_circle_hack ( this.slide_target, true );
	target = Math.round(target);
	this.$slides.removeClass(this.options.class_active);
	this.$slides.eq(target).addClass(this.options.class_active);
	this.$thumbs.removeClass(this.options.class_thumb_active);
	this.$thumbs.eq(target).addClass(this.options.class_thumb_active);
	if ( !this.options.carousel ) {
		if ( target <= this.options.display_edge[0] ) {
			this.$prev.addClass(this.options.class_prev_next_disabled);
		} else {
			this.$prev.removeClass(this.options.class_prev_next_disabled);
		}
		if ( target >= this.slides_count - this.options.display_edge[1] - 1 ) {
			this.$next.addClass(this.options.class_prev_next_disabled);
		} else {
			this.$next.removeClass(this.options.class_prev_next_disabled);
		}
	}
};


// JQUERY WRAPPER
$.fn.libslider = function ( type, options, option ) {
	var return_value;
	this.each ( (function () {
		if ( typeof (return_value) != 'undefined' ) return;
		if ( type == 'option' ) {
			var slider = LIBSLIDER.find_instance ( this );
			if ( typeof ( option ) == 'undefined' && typeof ( options ) == 'string' || typeof ( options ) == 'undefined' ) {
				return_value = slider.options_get ( options );
				return;
			}
			if ( typeof ( options ) == 'string' ) {
				var option_name = options;
				options = {};
				options[option_name] = option;
			}
			slider.options_merge( options );
		} else {
			var slider = new libslider ( this, type, options );
		}
	}));
	if ( typeof (return_value) != 'undefined' ) return return_value;
	return this;
}
