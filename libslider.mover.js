/* LIBSLIDER MOVER v 1.1
 *
 * Mover slider predefined type for LIBSLIDER
 *
 * by Volodymyr Golykov
 *
 * MIT License
 * Copyright (c) 2015 Volodymyr Golykov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 */
LIBSLIDER.types['mover'] = {
	move: 'x',				// Shift direction x - horizontal, y - vertical
	display: 1,
	init: function () {
		this.options.drag_swipe = this.options.move;
		this.options.display_edge = [this.options.display[0],this.options.display[1]];
		this.options.display[0] ++;
		this.slides_count_visible ++;
	},
	styles: function ( styles, slide ) {
		styles = {
			position: 'absolute',
			left: this.options.move == 'x' ? this.$slides.outerWidth(true) * slide : 0,
			top: this.options.move == 'y' ? this.$slides.outerHeight(true) * slide : 0,
			'z-index': slide < 0 ? 1 : this.slides_count - slide + 1
		};
		return styles;
	},
	drag: function ( slide_delta, drag ) {
		if ( this.options.move == 'x' ) {
			slide_delta = - drag.delta.x / this.$slides.outerWidth(true);
		} else {
			slide_delta = - drag.delta.y / this.$slides.outerHeight(true);
		}
		return slide_delta;
	}
};