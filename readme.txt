LibSlider
Extensible slider library for jQuery 1.7+
Author: Golykov Volodymyr.
Licence: MIT licence.

Documentation & Examples: http://libslider.com